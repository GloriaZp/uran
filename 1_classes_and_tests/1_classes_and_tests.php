<!DOCTYPE html>
<style>
    .color_block{
        width:50px;
        height:50px;
        margin:5px;
        float: left;
    }
</style>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <div class="block">

        <?php
            $colors = array('red', 'blue', 'green');
            $count_colors = array('red' => 0, 'blue' => 0, 'green' => 0);

            for($i = 0; $i < 1; $i++) {
                $number = mt_rand(0, count($colors) - 1);
                echo "<div class='color_block' style='background-color: " . $colors[$number] . "'></div>";
                $count_colors[$colors[$number]]++;
            }
            for($i = 1; $i < 2; $i++) {
                $number = mt_rand(0, count($colors) - 1);
                echo "<div class='color_block' style='background-color: " . $colors[$number] . "'></div>";
                $count_colors[$colors[$number]]++;
            }
            for($i = 2; $i < 3; $i++) {
                $number = mt_rand(0, count($colors) - 1);
                echo "<div class='color_block' style='background-color: " . $colors[$number] . "'></div>";
                $count_colors[$colors[$number]]++;
            }
            for($i = 3; $i < 4; $i++) {
                $number = mt_rand(0, count($colors) - 1);
                echo "<div class='color_block' style='clear:both; background-color: " . $colors[$number] . "'></div>";
                $count_colors[$colors[$number]]++;
            }
            for($i = 4; $i < 5; $i++) {
                $number = mt_rand(0, count($colors) - 1);
                echo "<div class='color_block' style='background-color: " . $colors[$number] . "'></div>";
                $count_colors[$colors[$number]]++;
            }
            for($i = 5; $i < 6; $i++) {
                $number = mt_rand(0, count($colors) - 1);
                echo "<div class='color_block' style='background-color: " . $colors[$number] . "'></div>";
                $count_colors[$colors[$number]]++;
            }
            for($i = 6; $i < 7; $i++) {
                $number = mt_rand(0, count($colors) - 1);
                echo "<div class='color_block' style='clear:both; background-color: " . $colors[$number] . "'></div>";
                $count_colors[$colors[$number]]++;
            }
            for($i = 7; $i < 8; $i++) {
                $number = mt_rand(0, count($colors) - 1);
                echo "<div class='color_block' style='background-color: " . $colors[$number] . "'></div>";
                $count_colors[$colors[$number]]++;
            }
            for($i = 8; $i < 9; $i++) {
                $number = mt_rand(0, count($colors) - 1);
                echo "<div class='color_block' style='background-color: " . $colors[$number] . "'></div>";
                $count_colors[$colors[$number]]++;
            }
            print_r($count_colors);
        ?>
    </div>
</body>
</html>
