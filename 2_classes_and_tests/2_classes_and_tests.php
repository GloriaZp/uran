<!DOCTYPE html>
<style>
    table {
        margin: 0 auto;
        margin-bottom: 5px;
    }
    tr:first-child {
        text-align: center;
    }
    .datehead {
        font-weight: 700;
    }
    tbody tr .cal {
        border: 1px solid #000;
        text-align: center;
        font-style: italic;
        border-radius: 10px;
    }
    tbody tr .datehead {
        text-align: center;
    }
    tbody tr .caltoday {
        border: 2px solid #000;
        text-align: center;
        font-weight: 700;
        font-style: italic;
        border-radius: 10px;
    }
    input {
        border-radius: 10px;
    }
    select {
        border-radius: 10px;
    }
</style>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <?php
    if(isset($_GET['month']))
        $month = $_GET['month'];
    elseif(isset($_GET['viewmonth']))
        $month = $_GET['viewmonth'];
    else $month = date('m');

    if(isset($_GET['year']))
        $year = $_GET['year'];
    elseif(isset($_GET['viewyear']))
        $year = $_GET['viewyear'];
    else $year = date('Y');

    if($month == '12')
        $next_year = $year + 1;
    else $next_year = $year;

    $Month_r = array(
        "1" => "Январь",
        "2" => "Февраль",
        "3" => "Март",
        "4" => "Апрель",
        "5" => "Май",
        "6" => "Июнь",
        "7" => "Июль",
        "8" => "Август",
        "9" => "Сентябрь",
        "10" => "Октябрь",
        "11" => "Ноябрь",
        "12" => "Декабрь");

    $first_of_month = mktime(0, 0, 0, $month, 1, $year);
    $day_headings = array('Sunday', 'Monday', 'Tuesday',
        'Wednesday', 'Thursday', 'Friday', 'Saturday');

    $maxdays = date('t', $first_of_month);
    $date_info = getdate($first_of_month);
    $month = $date_info['mon'];
    $year = $date_info['year'];

    if($month == '1'):
        $last_year = $year-1;
    else: $last_year = $year;
    endif;

    $timestamp_last_month = $first_of_month - (24*60*60);
    $last_month = date("m", $timestamp_last_month);

    if($month == '12')
        $next_month = '1';
    else $next_month = $month+1;

    $calendar = "
    <table width='390px' height='280px' style='border: 3px solid #cc281d; border-radius: 10px; font-weight: 500';>
        <tr style='background: linear-gradient(to right, #6e24ff, #f1da36); color: #fff'>
            <td colspan='7' class='navi'>
                <a style='margin-right: 50px; color: #50ed2a; font-weight: 600;'
                href='$self?month=" .$last_month."&year=".$last_year."'><<</a>
               ".$Month_r[$month]." ".$year."
                <a style='margin-left: 50px; color: #50ed2a; font-weight: 600;'
                href='$self?month=".$next_month."&year=".$next_year."'>>></a>
            </td>
        </tr>
        <tr>
            <td class='datehead'>Пн</td>
            <td class='datehead'>Вт</td>
            <td class='datehead'>Ср</td>
            <td class='datehead'>Чт</td>
            <td class='datehead'>Пт</td>
            <td class='datehead'>Сб</td>
            <td class='datehead'>Вс</td>
        </tr>
        <tr>";

    $class = "";

    $weekday = $date_info['wday'];
    $weekday = $weekday-1;
    if($weekday == -1) $weekday=6;

    $day = 1;

    if($weekday > 0)
        $calendar .= "<td colspan='$weekday'> </td>";

    while($day <= $maxdays)
    {
        if($weekday == 7) {
            $calendar .= "</tr><tr>";
            $weekday = 0;
        }
        $linkDate = mktime(0, 0, 0, $month, $day, $year);
        if((($day < 10 and "0$day" == date('d')) or ($day >= 10 and "$day" == date('d')))
            and (($month < 10 and "0$month" == date('m'))
                or ($month >= 10 and "$month" == date('m'))) and $year == date('Y'))
            $class = "caltoday";

        else {
            $d = date('m/d/Y', $linkDate);
            $class = "cal";
        }

        if($weekday == 5 || $weekday == 6) $red='style="color: red" ';
        else $red='';

        $calendar .= "
            <td class='{$class}'><span ".$red.">{$day}</span>
            </td>";
        $day++;
        $weekday++;
    }
    if($weekday != 7)
        $calendar .= "<td colspan='" . (7 - $weekday) . "'> </td>";
    echo $calendar . "</tr></table>";
    $months = array(
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь');

    echo "<form style='margin: 0 auto;width: 230px;' action='$self' method='get'>
    <select name='month'>";

    for($i=0; $i<=11; $i++) {
        echo "<option value='".($i+1)."'";
        if($month == $i+1)
            echo "selected = 'selected'";
        echo ">".$months[$i]."</option>";
    }

    echo "</select>";
    echo "<select name='year'>";

    for($i=date('Y')-200; $i<=(date('Y')+200); $i++)
    {
        $selected = ($year == $i ? "selected = 'selected'" : '');
        echo "<option value=\"".($i)."\"$selected>".$i."</option>";
    }

    echo "</select><input style='background: linear-gradient(to right, #6e24ff, #f1da36);' type='submit' value='переключить' /></form>";
    if($month != date('m') || $year != date('Y'))
        echo "<a style='float: left; margin-left: 10px; font-size: 12px; padding-top: 5px;'
        href='".$self."?month=".date('m')."&year=".date('Y')."'><-Текущая дата</a>";
    echo "</div>";
    ?>
</body>
</html>
